﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace PalindromeProducts
{
    class PalindromeProductTests
    {

        public PalindromeProduct sut { get; set; }

        [SetUp]
        public void Setup()
        {
            sut = new PalindromeProduct();
        }

        [Test]
        public void ValidatePalindrome_isValid()
        {
            var input = "aba";

            var result = sut.ValidatePalindrome(input);

            Assert.True(result);
        }

        [Test]
        public void ValidatePalindrome_isInvalid()
        {
            var input = "abb";

            var result = sut.ValidatePalindrome(input);

            Assert.False(result);
        }

        [Test]
        public void ValidatePalindromeNumber_isValid()
        {
            var input = 121;

            var result = sut.ValidatePalindrome(input);

            Assert.True(result);
        }

        [Test]
        public void ValidatePalindromeNumberSingleDigit_isValid()
        {
            var input = 1;

            var result = sut.ValidatePalindrome(input);

            Assert.True(result);
        }

        [Test]
        public void ValidatePalindromeProduct_isValid()
        {
            var inputA = 99;
            var inputB = 91;

            var result = sut.ValidatePalindromeProduct(inputA, inputB);

            Assert.True(result);
        }

        [Test]
        public void LargestPalindromProductByTwoDigits()
        {
            var largestPalindrome = 9009;

            var result = sut.FindLargestPalindrome(2);

            Assert.AreEqual(largestPalindrome, result);

        }

    }
}
