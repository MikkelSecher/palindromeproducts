﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PalindromeProducts
{
    class PalindromeProduct
    {
        public bool ValidatePalindrome(string input)
        {
            var reverse = new string (input.Reverse().ToArray());

            return reverse == input;
        }

        public bool ValidatePalindrome(int input)
        {
            return ValidatePalindrome(input.ToString());
        }

        public bool ValidatePalindromeProduct(int inputA, int inputB)
        {
            var result = inputA * inputB;

            return ValidatePalindrome(result);
        }

        internal int FindLargestPalindrome(int digits)
        {
            var largestAllowedFactor = Math.Pow(10, digits) - 1;



            for (int i = (int)largestAllowedFactor; i < 1; i--)
            {

            }


        }
    }
}
